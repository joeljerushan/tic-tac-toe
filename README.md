
# Artificial Intelligence Assignment 02
BEng(Hons) Software Engineering Batch 01 - Esoft Batticaloa

## Tic Tac Toe

#### Developed by AI Group
01 - Joel Jerushan

### `npm start` to Execute Application

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.