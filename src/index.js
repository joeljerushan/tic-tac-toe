import React from 'react';
import ReactDOM from 'react-dom';
import './app.css';


class Game extends React.Component {
  constructor(props) {
    super(props);
    
    this.handleResetButton = this.handleResetButton.bind(this);
    this.handleSinglePlayerButton = this.handleSinglePlayerButton.bind(this);
    this.handleMultiPlayerButton = this.handleMultiPlayerButton.bind(this);

    this.startMultiPlayerGameButton = this.startMultiPlayerGameButton.bind(this);
    this.handleChangeInput = this.handleChangeInput.bind(this);

    this.state = {
      vsPC: null,
      player_one_symbol: 'X',
      player_two_symbol: 'O',
      x_turn: true,
      board: ["", "", "", "", "", "", "", "", ""],
      player_one_name: '',
      player_two_name: '',
      multi_start: false
    };
  }

  

  handleBoxClick(index, keep_playing) {
    // if a box empty and game is not over yet do following action on each button click
    // also check user is single or multiplayer
    if (this.state.board[index] === "" && keep_playing === true && this.state.vsPC !== null) {
      let update_board = this.state.board;
      // if its multiplayer update icon and next turn
      if (this.state.vsPC === false) {
        let symbol = this.state.x_turn ? this.state.player_one_symbol : this.state.player_two_symbol;
        let next_turn = !this.state.x_turn;
        update_board[index] = symbol;
        // update the game state 
        this.setState({
          board: update_board,
          x_turn: next_turn, 
        });   
      }
      // if singleplayer 
      if (this.state.vsPC === true) {
        this.setState({
          player_one_name: 'You',
          player_two_name: 'PC'
        });
        update_board[index] = this.state.player_one_symbol;
        // update the game state 
        this.setState({board: update_board});
        let ai_index = find_best_move(update_board);
        if (ai_index !== -4) update_board[ai_index] = this.state.player_two_symbol; 
        this.setState({ board: update_board });
      }
    }
  }

  handleResetButton() {
    this.setState({
      board: ["", "", "", "", "", "", "", "", ""],
      x_turn: true
    });
  }

  handleRestartButton(){
    window.location.reload();
  }

  handleSinglePlayerButton() {
    if (this.state.vsPC === null) 
      this.setState({vsPC: true});
  }

  handleMultiPlayerButton() {
    if (this.state.vsPC === null) 
      this.setState({vsPC: false});
  }

  //show draw message 
  showDrawMessage(is_draw){
    if(is_draw === true) {
      return(
        <div className="draw_message">
          <span></span>
            <h1>Match Draw</h1>
        </div>
      );
    }
  }

  //show win message and winner
  showWinMessage(win, winner){
    if(win === true) {
      return(
        <div className="winner_message">
          <span></span>
            <h1>Winner ! { winner==='X' ? this.state.player_one_name : this.state.player_two_name }</h1>
        </div>
      );
    }
  }

  handleChangeInput(event) {

    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
        [name]: value,
    }); 
  }
    
  startMultiPlayerGameButton(){
    //event.preventDefault();
    this.setState({
      multi_start: true,
    }); 
  }

  //checking game player function
  checkMultiPlayer(vsPc, keep_playing){
    //if player going to play with computer
    if(vsPc === true) {
      return (
        //show game board
        <div className={this.state.vsPC === null ? "game deactived-board" : "game activated-board"}>
            <div className="board">
              {this.state.board.map((cell, index) => {
                return <div className="square" key={index} onClick={() => this.handleBoxClick(index, keep_playing)}> {cell} </div>
              })}
            </div>
        </div> 
      );
    }
    //if player going to play with another user
    if(vsPc === false){
      let player_one = this.state.player_one_name
      let player_two = this.state.player_two_name
      let start_game = this.state.multi_start

      //Checking & Validating user name
      if(player_one.length > 3 && player_two.length > 3 && start_game === true){
        return(
          //show game board
          <div className={this.state.vsPC === null ? "game deactived-board" : "game activated-board"}>
              <div className="board">
                {this.state.board.map((cell, index) => {
                  return <div className="square" key={index} onClick={() => this.handleBoxClick(index, keep_playing)}> {cell} </div>
                })}
              </div>
          </div> 
        );
      } else {
        return(
          //if there is no user name and given user name is invalid ask for player names 
          <div>
              <form>
                <div className="request-name">
                    <div className="input-row">
                        <label>Player One (X)</label>
                        <input type="text" value={this.state.player_one_name} name="player_one_name" onChange={this.handleChangeInput}/>
                    </div>
                    <div className="input-row">
                        <label>Player Two (O)</label>
                        <input type="text" value={this.state.player_two_name} name="player_two_name" onChange={this.handleChangeInput}/>
                    </div>
                    <div className="input-row">
                        <button type="button" onClick={this.startMultiPlayerGameButton}>Start Game</button>
                    </div>
                </div>
              </form>
          </div>
        );
      }
    }
  }


  render() {
    //always check is there any winner on board
    let have_winner = winner(this.state.board);

    //set game state to keep play
    let keep_playing = have_winner === null ? true : false;
    
    //checking match draw or not by passing current board state
    let check_draw = draw(this.state.board)
    //assign false value before its draw
    let is_draw = false 
    //setting value to true when draw function return true and there is no winner left
    if(check_draw === true && have_winner === null) {
      is_draw = true 
    }

    //check winner and mark win state 
    let win = false
    if (have_winner !== null) {
      win = true 
    }

    return (
      //rendering game board 
      <div className="master">
        <div className="title">
          <h1 className={this.state.vsPC === null ? "" : "reduce-margin"} >Tic <span>*</span> Tac <span>*</span> Toe</h1>
        </div>
        <div className="button_navigations">
            <div className={this.state.vsPC === null ? "active-button" : "deactived-button"} 
            onClick={this.handleSinglePlayerButton}><span className="splash"></span> Single Player </div>
            <div className={this.state.vsPC === null ? "active-button" : "deactived-button"} 
            onClick={this.handleMultiPlayerButton}><span className="splash"></span> Multiplayer </div>
        </div>
        <div className={this.state.vsPC === null ? "deactived-board update-game" : "activated-board update-game"}>
          <div className="update-button" onClick={this.handleResetButton}> Start Again </div>
          <div className="update-button" onClick={this.handleRestartButton}> Main Menu </div>
        </div>
        { this.showDrawMessage(is_draw) }
        { this.showWinMessage(win, have_winner) }
        { this.checkMultiPlayer(this.state.vsPC, keep_playing) }
      </div>
    );
  }
}

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);


//check draw function
function draw(squares){
  if(!squares.includes("")){
    return true
  }
}

//check winner
function winner(squares) {
  //arraw set of winners 
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];
  //looping win states 
  for (let i = 0; i < lines.length; i++) {
    //assign win state to three selectable array
    let [a, b, c] = lines[i];
    //checking if a or b or c has win state
    if (squares[a] !== "" && squares[a] === squares[b] && squares[a] === squares[c] && squares[b] === squares[c])
      return squares[a];
  }
  return null;
}

function arrayToMat(squares) {
  let mat = []
  let k = 0;

  for (let i = 0; i < 3; i++) {
    mat[i] = [];
    for (let j = 0; j < 3; j++) mat[i][j] = squares[k++];
  }

  return mat;
}

// If it has an empty space, keep playing
function hasMovesLeft(mat) {
  for (let i = 0; i < 3; i++) {
    for (let j = 0; j < 3; j++) {
      if (mat[i][j] === "") return true;
    }
  }
  //else return false
  return false;
}

//check is there any moves 
function evaluate(mat, depth) {

  // Check every row
  for (let i = 0; i < 3; i++) {
    if (mat[i][0] === mat[i][1] && mat[i][0] === mat[i][2] && mat[i][1] === mat[i][2]) {
      if (mat[i][0] === 'O') return 100 - depth;
      if (mat[i][0] === 'X') return depth - 100;
    }
  }

  // Check every col
  for (let j = 0; j < 3; j++) {
    if (mat[0][j] === mat[1][j] && mat[0][j] === mat[2][j] && mat[1][j] === mat[2][j]) {
      if (mat[0][j] === 'O') return 100 - depth;
      if (mat[0][j] === 'X') return depth - 100;
    }
  }

  // Check the diagonals
  if (mat[0][0] === mat[1][1] && mat[0][0] === mat[2][2] && mat[1][1] === mat[2][2]) {
    if (mat[0][0] === 'O') return 100 - depth;
    if (mat[0][0] === 'X') return depth - 100;
  }

  if (mat[0][2] === mat[1][1] && mat[0][2] === mat[2][0] && mat[1][1] === mat[2][0]) {
    if (mat[0][2] === 'O') return 100 - depth;
    if (mat[0][2] === 'X') return depth - 100;
  }

  // If the game hasn't finished yet
  return 0;
}

//minmax function
//and its requires 3 parameters mat, depth, get_max
function minmax(mat, depth, get_max) {
  if (hasMovesLeft(mat) === false) {
    return evaluate(mat, depth);    
  }
  let val = evaluate(mat, depth);
  if (val !== 0) return val;
  if (get_max) {
    let best = -Infinity;
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        if (mat[i][j] === "") {
          mat[i][j] = 'O';
          best = Math.max(best, minmax(mat, depth+1, !get_max));
          mat[i][j] = "";
        }
      }
    }
    return best;
  }
  else {
    let best = Infinity;
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        if (mat[i][j] === "") {
          mat[i][j] = 'X';
          best = Math.min(best, minmax(mat, depth+1, !get_max));
          mat[i][j] = "";
        }
      }
    }
    return best;
  }
}

//find_best_move
function find_best_move(squares) {
  let mat = arrayToMat(squares);
  let val, row = -1, col = -1, best = -Infinity;
  //loop to find next move
  for (let i = 0; i < 3; i++) {
    for (let j = 0; j < 3; j++) {
      if (mat[i][j] === "") {
        mat[i][j] = 'O';
        val = minmax(mat, 0, false);
        mat[i][j] = "";

        if (val > best) {
          best = val;
          row = i;
          col = j;
        }
      }
    }
  }
  //return predected moves
  return (3 * row) + col;
}